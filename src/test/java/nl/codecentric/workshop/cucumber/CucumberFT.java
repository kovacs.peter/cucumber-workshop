package nl.codecentric.workshop.cucumber;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        glue = {"nl.codecentric.workshop.cucumber.definition"},
        features = {"classpath:features/triangle-calculation.feature"},
        format = {"pretty", "html:target/site/cucumber-pretty", "json:target/cucumber-access-rest-api.json"})
public class CucumberFT {
}
